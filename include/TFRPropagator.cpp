/*!
 *  @author    Simon Stemmle
 *  @brief     Extrapolator of a track state to a given z position
 */

#include "TFRPropagator.h"

using namespace std;

//----------
// Extrapolate a state to a given z position
//----------
bool TFRPropagator::PropagateState(TFRState *state, double z){

  //Get the layers which are between initial and final z (state->GetZ() <= z_layer <z)
  TFRLayers layers = m_geo->GetLayersBetween(state->GetZ(), z);
 
  //Do the extrapolation from layer to layer
  for(int i = 0; i < layers.GetEntries(); i++){
    
    //TASK: extrapolate the state to the next layer (without multiple scattering) and add the scattering effect to the covariance
    
  }

  //TASK: last step of the extrapolation, to the final z after the last layer
  //      Should you consider multiple scattering here?
  
  return true;
}

//----------
// Extrapolate a state to a given z position without any scattering inbetween
//----------
bool TFRPropagator::PropagateStateNoScattering(TFRState *state, double z){

  //If we are already there we have nothing to do
  if(z==(state->GetZ())) return true;

  //Determine wether we are propagating fowards or backwards
  bool forward = z > state->GetZ();

  //Check wether there is a magnetic field change inbetween (entering and/or leaving it)
  if(forward){

    //If the magnetic field will come at larger z
    if(m_geo->GetBFieldStartZ() >= state->GetZ()){
      PropagateStateStraight(state, min(z,m_geo->GetBFieldStartZ()));
      
      //inside the possible B-field
      if(!PropagateStateInBField(state, min(z,m_geo->GetBFieldEndZ()))) return false;
    }
    //If we are already in the B-field
    else if(m_geo->GetBFieldEndZ() >= state->GetZ()){
      if(!PropagateStateInBField(state, min(z,m_geo->GetBFieldEndZ()))) return false;
    }

    //Straight in the possible rest after the B-field
    PropagateStateStraight(state, z);
  }
  else{
    //If the B-field will come at smaller z
    if(m_geo->GetBFieldEndZ() <= state->GetZ()){
      PropagateStateStraight(state, max(z,m_geo->GetBFieldEndZ()));
      
      //inside the possible B-field
      if(!PropagateStateInBField(state, max(z,m_geo->GetBFieldStartZ())))
        return false;
    }
    //If we are already in the B-field
    else if(m_geo->GetBFieldStartZ() <= state->GetZ()){
      if(!PropagateStateInBField(state, max(z,m_geo->GetBFieldStartZ()))) return false;
    }
    
    //Straight in the possible rest after the B-field
    PropagateStateStraight(state, z);
    
  }
  
  return true;
}
//----------
// Extrapolate a state to a given z position (straight line)
//----------
void TFRPropagator::PropagateStateStraight(TFRState *state, double z){
  
  //Distance in z to be extrapolated
  double dz = z-state->GetZ();
  
  //If we are already there, we have nothing to do
  if(dz==0) return;

  //Transportation matrix: x_new=F*x_old
  //Array representing this matrix
  double FtmpA[5*5];

  //TASK: fill the transportation matrix (Jacobian)

  TMatrixD Ftmp(5,5,FtmpA);
  
  //Transposed matrix
  TMatrixD FtmpT(TMatrixD::kTransposed, Ftmp);

  //Transport the statevector with a stright line
  state->SetStateVect(Ftmp * (state->GetStateVect()));
  
  //Transport the covariance matrix using the same transportation matrix
  state->SetP(Ftmp * (state->GetP()) * FtmpT); 
  
  //Set the new z position
  state->SetZ(z);

  return;
}

//----------
// Extrapolate a state to a given z position (in constant B-field)
//----------
bool TFRPropagator::PropagateStateInBField(TFRState *state, double z){
  
  //distance in z
  double dz = z-state->GetZ();
  
  //If we are already there we have nothing to do
  if(dz==0) return true;
  
  //if B-field strength is zero: do the straight line extrapolation
  if(m_geo->GetBMag()==0){
    PropagateStateStraight(state, z);
    return true;
  }
  
  //The state vector of the extrapolated state
  TVectorD state_new(5);

  //Current state vector
  TVectorD state_vect = state->GetStateVect();

  //TASK: compute the new state
  
  //Set the new state vector
  state->SetStateVect(state_new);
  
  //determine the transport matrix (derivatives of the formulas above)
  TMatrixD Ftmp(5,5);
  
  //TASK: fill the transportation matrix (Jacobian)
  
  //Transposed transport matrix
  TMatrixD FtmpT(TMatrixD::kTransposed, Ftmp);

  //Set the transported covariance matrix
  state->SetP(Ftmp * (state->GetP()) * FtmpT);
 
  //Set the new z position
  state->SetZ(z);
 
  return true;
}

//----------
// Add scattering to covariance matrix
//----------
bool TFRPropagator::AddScatteringToCovariance(TFRState *state, TFRLayer *layer){
  
  //check wether we are still inside the detector
  double x = (state->GetStateVect())[0];
  double y = (state->GetStateVect())[1];
  double xmax = layer->GetXMax();
  double xmin = layer->GetXMin();
  double ymax = layer->GetYMax();
  double ymin = layer->GetYMin();
  
  if(x < xmin || x > xmax || y < ymin || y > ymax) return false;

  //Check wether we should calculate the effect of MS at this point.
  if(!m_useMs || !m_geo->GetMultipleScattStatus()) return true;

  //Get multiple scattering factor (how much material)
  double XoverXZero = layer->GetXoverXZero();
  
  //Calculate the expected angular deflection
  double momentum = fabs(1.0/(state->GetStateVect())[4]);
  
  //assume kaon mass
  double beta = sqrt(1. - pow(493.7/momentum, 2));
  
  double angleRms = m_geo->GetMultipleScattering()->GetThetaRMSGauss(XoverXZero, beta, momentum, 1); 
  
  //Translate this angle in variations of tx and ty, tan(alpah) = x/z = tx (Good enough approximation also for the 2D scattering case)
  double txRms2 = pow((1+pow((state->GetStateVect())[2],2))*angleRms,2);
  double tyRms2 = pow((1+pow((state->GetStateVect())[3],2))*angleRms,2);//TODO
  
  //Noise matrix to be added to the covariance
  double q[5*5]={0,0,       0,       0,0,
                 0,0,       0,       0,0,
                 0,0,  txRms2,       0,0,
                 0,0,       0,  tyRms2,0,
                 0,0,       0,       0,0};
  
  TMatrixD Q(5,5,q);
  
  state->SetP(state->GetP() + Q);
  
  return true;
}





